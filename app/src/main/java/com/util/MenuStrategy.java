package com.util;

/**
 * 头部文件中 右边按钮的策略模式
 * @author Administrator
 *
 */
public interface MenuStrategy {
	void openActivity();
}
