package com.util;

import com.maincode.WCCBaseActivity;

/**
 * Created by djx on 16/1/13.
 */
public class HttpUrl {
    /*图片路劲*/
    public static final String PICURL = "http://wcc-pic.oss-cn-hangzhou.aliyuncs.com";
    /*接口地址*/
    private static String url = "http://120.27.196.221:8080";
    /*注册-发送短信验证码*/
    public static final String SENDSMSCODE = url + "/api/sendSmscode.do";
    /*注册-验证电话号码可用*/
    public static final String AVAILABLEPHONE = url + "/api/availablePhone.do";
    /*注册-账户密码*/
//    public static final String REGISTBYSMSCODE = url + "/api/registBySmscode.do";
    /*登录[玩车车账号密码]*/
    public static final String LOGIN = url + "/api/login.do";
    /*刷新 token*/
    public static final String REFRESHTOKEN = url + "/api/refreshToken.do";
    /*新增动态*/
    public static final String SAVE = url + "/api/user/moments/save.do";
    /*上传图片*/
    public static final String UPDATEPIC = url + "/api/user/uploadPic.do";
    /*附近动态*/
    public static final String NEARBY = url + "/api/user/moments/nearby.do";
    /*动态-赞*/
    public static final String LIKE = url + "/api/user/moments/like.do";
    /*动态-评论*/
    public static final String COMMENT = url + "/api/user/moments/comment.do";
    /*动态-评论列表*/
    public static final String COMMENTS = url + "/api/user/moments/comments.do";
    /*动态-上传图片*/
    public static final String UPLOADPHOTO = url + "/api/user/moments/uploadPhoto.do";
    /*附近人*/
    public static final String USERNEARBY = url + "/api/user/nearby.do";
    /*通过品牌获取车型*/
    public static final String GETBYBRAND = url + "/api/car/series/getByBrand.do";
    /*获取品牌列表*/
    public static final String CAR_BRAND = url + "/api/car/brand/list.do";
    /*爱车信息添加*/
    public static final String ADDCAR = url + "/api/car/add.do";
    /*注册-注册最后一步，生成账号*/
    public static final String REGISTBYSMSCODE = url + "/api/registBySmscode.do";
    /*用户位置报告*/
    public static final String REPORTPOSITION = url + "/api/user/reportPosition.do";
    /*注册-验证验证码*/
    public static final String SMSCODE = url + "/api/validSmscode.do";
    /*获取我的个人资料*/
    public static final String FULLPROFILE = url + "/api/user/fullProfile.do";
    /*清除新访问日志*/
    public static final String CLEARVIEWDLOG = url + "/api/user/clearViewedLog.do";

    public static String getUrl(String u) {
        return (u + "?t=" + WCCBaseActivity.token);
//        return (u + "?t=385409c7-01a4-4d6c-8b7a-e0e46ee7f123");
    }
}
