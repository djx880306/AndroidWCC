package com.util;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.risen.androidwcc.R;
import com.maincode.model.ImgBean;
import com.view.AdapterManager;
import com.view.GrideViewImgListView;
import com.view.HorizontallImgListView;
import com.view.RoundAngleImageView;

/**
 * 圆角图片适配器
 * 
 * @author djx
 * 
 */
public class RoundImgHorizontalAdapter extends AdapterManager<ImgBean> {

	private boolean hasDelete = true;
	private GrideViewImgListView grideViewImgListView;
	private HorizontallImgListView horizontallImgListView;
	
	private boolean is_read;

	public void setHasDelete(boolean hasDelete) {
		this.hasDelete = hasDelete;
	}

	public RoundImgHorizontalAdapter(List<ImgBean> list, Context context) {
		super(list, context);
		this.bu.configDefaultLoadFailedImage(R.mipmap.img_default);
		this.bu.configDefaultLoadingImage(R.mipmap.img_default);
	}
	
	public RoundImgHorizontalAdapter(List<ImgBean> list, Context context, boolean is_read) {
		super(list, context, is_read);
		this.bu.configDefaultLoadFailedImage(R.mipmap.bg_wcc80);
		this.bu.configDefaultLoadingImage(R.mipmap.bg_wcc80);
		this.is_read = is_read;
	}

	public RoundImgHorizontalAdapter(List<ImgBean> list, Context context, GrideViewImgListView grideViewImgListView) {
		super(list, context);
		this.bu.configDefaultLoadFailedImage(R.mipmap.img_default);
		this.bu.configDefaultLoadingImage(R.mipmap.img_default);
		this.grideViewImgListView = grideViewImgListView;
	}
	public RoundImgHorizontalAdapter(List<ImgBean> list, Context context, HorizontallImgListView horizontallImgListView) {
		super(list, context);
		this.bu.configDefaultLoadFailedImage(R.mipmap.img_default);
		this.bu.configDefaultLoadingImage(R.mipmap.img_default);
		this.horizontallImgListView = horizontallImgListView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.i("tag", "position===>" + position);
		if (convertView == null) {
			convertView = this.inflater.inflate(R.layout.round_iamge_item, parent, false);
		}
		int totalWidth = Tools.getScreenWidth(context);
		int width = totalWidth / 4;
		int line = Tools.dip2px(context, 10);

		RoundAngleImageView imgImg = ViewHolder.get(convertView, R.id.img_img);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgImg.getLayoutParams();
		params.width = width - line;
		params.height = width - line;
		ImageView imgDelete = ViewHolder.get(convertView, R.id.img_delete);
		TextView txtValue = ViewHolder.get(convertView, R.id.txt_values);
		final ImgBean bean = getItem(position);
		txtValue.setText(bean.getValue());
		this.bu.display(imgImg, bean.getPath());
		final int index = position;
		imgDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (bean.getPath() != null && !bean.getPath().equals("")) {
					RoundImgHorizontalAdapter.this.deleteFromIndex(index);
					if(grideViewImgListView != null && !grideViewImgListView.hasDefaultImg()){
						addData(new ImgBean("", ""));
					}
					if(horizontallImgListView != null && !horizontallImgListView.hasDefaultImg()){
						addData(new ImgBean("", ""));
					}
				}
			}
		});
		if (Tools.StringHasContent(bean.getPath()) && hasDelete) {
			imgDelete.setVisibility(View.VISIBLE);
		} else {
			imgDelete.setVisibility(View.GONE);
		}
		return convertView;
	}
}
