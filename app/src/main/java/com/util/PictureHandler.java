package com.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.maincode.WCCBaseActivity;
import com.maincode.model.ImageObj;
import com.maincode.model.ImgBean;
import com.maincode.model.ImgUploadBean;
import com.maincode.model.Result;
import com.maincode.model.UploadBean;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;

/**
 * 图片处理类 这里主要在线程中对图片进行大小和比例的缩小和图片上传
 *
 * @author Administrator
 */
public abstract class PictureHandler {
    private Context context;
    private RequestParams params;
    private Object obj = new Object();
    private List<String> fileNameList;
    private List<ImgBean> beans;

    public PictureHandler(Context context) {
        this.context = context;
        params = new RequestParams();
        fileNameList = new ArrayList<String>();
        WCCBaseActivity activity = (WCCBaseActivity) context;
        activity.showDialog(context, "正在处理图片");
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            uploadImg();
        }
    };

    /**
     * 上传图片
     */
    private void uploadImg() {
        HttpUtils utils = HttpUtils.getInstance();
        String url = HttpUrl.getUrl(HttpUrl.UPLOADPHOTO);
        utils.send(HttpMethod.POST, url, params, new HttpRequestCallBack<ImageObj>(ImageObj.class, context) {

            @Override
            protected void onSuccess(ImageObj t) {
                PictureHandler.this.onSuccess(t);
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
            }
        });
    }

    /**
     * 解析网络的logo
     *
     * @return
     */
    public String formatHttpImg() {
        String value = "";
        for (ImgBean img : PictureHandler.this.beans) {
            if (Tools.StringHasContent(img.getPath()) && img.getPath().startsWith("http")) {
                value += img.getPath() + ",";
            }
        }
        return value;
    }

    /**
     * 处理图片
     *
     * @param list
     * @param width
     * @param height
     */
    public void handlerPicture(List<ImgBean> list, int width, int height, String saveFilePath) {
        WCCBaseActivity activity = (WCCBaseActivity) this.context;
        PictureHandler.this.beans = list;
        if (list == null || list.isEmpty() || !hasLocalPic(list)) {
            Result result = new Result();
            String localValue = formatHttpImg();
            if (localValue.lastIndexOf(",") >= 0) {
                localValue = localValue.substring(0, localValue.lastIndexOf(","));
            }
            result.setData(localValue);
            onSuccess(result);
            onSuccess(new ImageObj());
//            activity.dismissDialog();
            return;
        }
        Thread thread = new Thread(new MyRunnable(PictureHandler.this.beans, width, height, saveFilePath));
        thread.start();
    }

    private boolean hasLocalPic(List<ImgBean> list) {
        for (ImgBean bean : list) {
            if (Tools.StringHasContent(bean.getPath()) && !bean.getPath().contains("http")) {
                return true;
            }
        }
        return false;
    }

    public class MyRunnable implements Runnable {
        private List<ImgBean> list;
        private int width;
        private int height;
        private String saveFilePath;

        public MyRunnable(List<ImgBean> list, int width, int height, String saveFilePath) {
            this.list = list;
            this.width = width;
            this.height = height;
            this.saveFilePath = saveFilePath;

        }

        @Override
        public void run() {
            synchronized (obj) {
                int count = 0;
                for (ImgBean bean : list) {
                    if (Tools.StringHasContent(bean.getPath()) && !bean.getPath().contains("http")) {
                        String path = BitmapTools.compressWidthAndHeight(bean.getPath(), width, height, saveFilePath);
                        params.addBodyParameter("pic", new File(path));
                        fileNameList.add(path);
                        count++;
                    }
                }
//                params.addBodyParameter("t", WCCBaseActivity.token);

                handler.sendEmptyMessage(0);
            }
        }
    }

    public abstract void onSuccess(Result value);

    public abstract void onSuccess(ImageObj value);
}
