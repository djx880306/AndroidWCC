package com.view;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.util.Tools;

public class TimerDialog extends Dialog {

	public TimerDialog(Context context, int theme) {
		super(context, theme);
	}

	public static class Buildler {
		private Context context;
		@ViewInject(R.id.time_year)
		private WheelView time_year;
		@ViewInject(R.id.time_month)
		private WheelView time_month;
		@ViewInject(R.id.time_day)
		private WheelView time_day;
		@ViewInject(R.id.btn_ok)
		private Button btn_ok;
		private List<String> yearList;
		private List<String> monthList;
		private List<String> dayList;
		private String year;
		private String month;
		private String day;
		private OnDateCallBack back;
		private String currentData;

		public void setBack(OnDateCallBack back) {
			this.back = back;
		}

		public Buildler(Context context) {
			this.context = context;
		}

		public interface OnDateCallBack {
			void getDate(String year, String month, String day);
		}

		public TimerDialog create() {
			final TimerDialog timerDialog = new TimerDialog(context, R.style.MyDialog);
			View currentView = LayoutInflater.from(context).inflate(R.layout.car_timer_dialog, null);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			timerDialog.setContentView(currentView, params);
			ViewUtils.inject(this, currentView);
			// time_year.set
			// 添加change事件
			time_year.addChangingListener(new OnWheelChangedListener() {

				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					year = yearList.get(time_year.getCurrentItem());
				}
			});
			// 添加change事件
			time_month.addChangingListener(new OnWheelChangedListener() {

				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					month = monthList.get(time_month.getCurrentItem());
					List<String> tempList = getday(month);
					time_day.setAdapter(new MyAdapter(tempList));
					if (time_day.getCurrentItem() >= tempList.size()) {
						time_day.setCurrentItem(tempList.size() - 1);
					}
					day = dayList.get(time_day.getCurrentItem());
				}
			});
			// 添加change事件
			time_day.addChangingListener(new OnWheelChangedListener() {

				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					day = dayList.get(time_day.getCurrentItem());
				}
			});
			btn_ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					timerDialog.dismiss();
					if (back != null) {
						back.getDate(year, formatValue(month), formatValue(day));
					}
				}
			});
			if (!Tools.StringHasContent(currentData)) {
				currentData = getCurrentDate();
			}
			String[] str = currentData.split("-");
			year = str[0];
			time_year.setVisibleItems(7);
			time_year.setLabel("年");
			time_year.setCyclic(true);
			time_year.setAdapter(new MyAdapter(getYear()));
			time_year.setCurrentItem(getCurrentYear(year));

			month = str[1];
			time_month.setVisibleItems(7);
			time_month.setLabel("月");
			time_month.setCyclic(true);
			time_month.setAdapter(new MyAdapter(getMonth()));
			time_month.setCurrentItem(getCurrentMonth(month));

			day = str[2];
			time_day.setVisibleItems(7);
			time_day.setLabel("日");
			time_day.setCyclic(true);
			time_day.setAdapter(new MyAdapter(getday(month)));
			time_day.setCurrentItem(getCurrentDay(day));

			Window win = timerDialog.getWindow();
			win.setGravity(Gravity.BOTTOM);
			Activity activity = (Activity) context;
			WindowManager windowManager = activity.getWindowManager();
			Display display = windowManager.getDefaultDisplay();
			WindowManager.LayoutParams lp = timerDialog.getWindow().getAttributes();
			lp.width = (int) (display.getWidth()); // 设置宽度
			return timerDialog;
		}

		public void setCurrentTime(String time) {
			if (Tools.judgeTimeYMD(time)) {
				this.currentData = time;
			}
		}

		/**
		 * 格式化日期
		 * 
		 * @return
		 */
		private String formatValue(String value) {
			DecimalFormat format = new DecimalFormat("00");
			return format.format(Double.parseDouble(value));
		}

		private String getCurrentDate() {
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return currentData = dateFormat.format(date);
		}

		private int getCurrentYear(String year) {
			for (int i = 0; i < yearList.size(); i++) {
				if (year.equals(yearList.get(i))) {
					return i;
				}
			}
			return 0;
		}

		private int getCurrentMonth(String month) {
			try {
				int tempMonth = Integer.parseInt(month);
				String strTempMonth = String.valueOf(tempMonth);
				for (int i = 0; i < monthList.size(); i++) {
					if (strTempMonth.equals(monthList.get(i))) {
						return i;
					}
				}
				return 0;
			} catch (Exception e) {
				return 0;
			}

		}

		private int getCurrentDay(String day) {
			try {
				int tempDay = Integer.parseInt(day);
				String strTempDay = String.valueOf(tempDay);
				for (int i = 0; i < dayList.size(); i++) {
					if (strTempDay.equals(dayList.get(i))) {
						return i;
					}
				}
				return 0;
			} catch (Exception e) {
				return 0;
			}
		}

		private List<String> getYear() {
			yearList = new ArrayList<String>();
			for (int i = 1900; i < 2055; i++) {
				yearList.add(i + "");
			}
			return yearList;
		}

		private List<String> getMonth() {
			monthList = new ArrayList<String>();
			for (int i = 1; i <= 12; i++) {
				monthList.add(i + "");
			}
			return monthList;
		}

		private List<String> getday(String value) {
			int month = Integer.parseInt(value);
			dayList = new ArrayList<String>();
			if (month == 2) {
				if ((month % 4 == 0 && month % 100 != 0) || month % 400 == 0) {
					for (int i = 1; i <= 29; i++) {
						dayList.add(i + "");
					}
				} else {
					for (int i = 1; i <= 28; i++) {
						dayList.add(i + "");
					}
				}
			} else {
				if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
					for (int i = 1; i <= 31; i++) {
						dayList.add(i + "");
					}
				} else {
					for (int i = 1; i <= 30; i++) {
						dayList.add(i + "");
					}
				}
			}
			return dayList;
		}

		public class MyAdapter implements WheelAdapter {
			public List<String> list;

			private MyAdapter(List<String> list) {
				this.list = list;
			}

			@Override
			public int getItemsCount() {
				return list.size();
			}

			@Override
			public String getItem(int index) {
				return list.get(index);
			}

			@Override
			public int getMaximumLength() {
				return 0;
			}

		}
	}
}
