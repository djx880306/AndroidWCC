package com.view;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.util.ViewHolder;

public class CarTypeViewAdapter extends BaseExpandableListAdapter{

	private List<String> groupList ;
	private List<CarSeries> childList;
	private LayoutInflater inflater;
	private BitmapUtils bu;
	public CarTypeViewAdapter(List<String> groupList , List<CarSeries> childList, Context context){
		this.childList = childList;
		this.groupList = groupList;
		this.inflater = LayoutInflater.from(context);
		bu = new BitmapUtils(context);
		bu.configDefaultLoadFailedImage(R.mipmap.bg_wcc80);
		bu.configDefaultLoadingImage(R.mipmap.bg_wcc80);
	}
	@Override
	public Object getChild(int arg0, int arg1) {
		return childList.get(arg0);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		return arg1;
	}

	@Override
	public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
		if(arg3 == null){
			arg3 = this.inflater.inflate(R.layout.car_type_view_adapter_chilid, null);
		}
		ImageView img_head = ViewHolder.get(arg3, R.id.img_head);
		TextView txt_carName = ViewHolder.get(arg3, R.id.txt_carName);
		CarSeries bean = this.childList.get(arg1);
		if(!TextUtils.isEmpty(bean.getLogo())){
			bu.display(img_head, bean.getLogo());
		}
		txt_carName.setText(bean.getName());
		txt_carName.setTextSize(16);
		return arg3;
	}

	@Override
	public int getChildrenCount(int arg0) {
		return childList.size();
	}

	@Override
	public Object getGroup(int arg0) {
		return groupList.get(arg0);
	}

	@Override
	public int getGroupCount() {
		return groupList.size();
	}

	@Override
	public long getGroupId(int arg0) {
		return arg0;
	}

	@Override
	public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
		if(arg2 == null){
			arg2 = this.inflater.inflate(R.layout.car_type_view_adapter_group, null);
		}
		return arg2;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

}
