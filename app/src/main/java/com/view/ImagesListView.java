package com.view;

import java.util.ArrayList;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.maincode.WccApplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ImagesListView extends Activity {

	private ViewPager viewpager;
	ArrayList<View> viewContainter = new ArrayList<View>();
	
	public WccApplication app() {
		return (WccApplication) getApplication();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.car_image_list);
		viewpager = (ViewPager) this.findViewById(R.id.viewpager);
		
		Bundle bl = getIntent().getBundleExtra("bl");
		ArrayList<String> pathArr = bl.getStringArrayList("img_list");
		
		int index = getIntent().getIntExtra("index", 0);
		
		LayoutParams layout = new LayoutParams();
		layout.height = LayoutParams.MATCH_PARENT;
		layout.width = LayoutParams.MATCH_PARENT;
		
//		View view;
		for (int i = 0; i < pathArr.size(); i++) {
//			view = LayoutInflater.from(this).inflate(R.layout.car_images_layout, null);
			
			ImageView imageView = new ImageView(this);//(ImageView) view.findViewById(R.id.img_view);
			imageView.setLayoutParams(layout);
			imageView.setScaleType(ScaleType.CENTER_INSIDE);
			final PhotoViewAttacher mAttacher = new PhotoViewAttacher(imageView);
			mAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
				
				@Override
				public void onPhotoTap(View arg0, float arg1, float arg2) {
					finish();
				}
			});
			BitmapUtils bitmapUtils = new BitmapUtils(this);
			bitmapUtils.display(imageView, pathArr.get(i), new BitmapLoadCallBack<ImageView>() {

				@Override
				public void onLoadCompleted(ImageView arg0, String arg1,
						Bitmap arg2, BitmapDisplayConfig arg3,
						BitmapLoadFrom arg4) {
					arg0.setImageBitmap(arg2);
					mAttacher.update();
				}

				@Override
				public void onLoadFailed(ImageView arg0, String arg1, Drawable arg2) {
					// TODO Auto-generated method stub
					
				}
			});
			bitmapUtils.display(imageView, pathArr.get(i));
			
			viewContainter.add(imageView);
		}
		
		viewpager.setAdapter(new PagerAdapter() {

			// viewpager中的组件数量
			@Override
			public int getCount() {
				return viewContainter.size();
			}

			// 滑动切换的时候销毁当前的组件
			@Override
			public void destroyItem(View container, int position,
					Object object) {
				((ViewPager) container).removeView(viewContainter.get(position));
			}

			// 每次滑动的时候生成的组件
			@Override
			public Object instantiateItem(View container, int position) {
				((ViewPager) container).addView(viewContainter.get(position));
				return viewContainter.get(position);
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public int getItemPosition(Object object) {
				return super.getItemPosition(object);
			}

			@Override
			public CharSequence getPageTitle(int position) {
				return "";
			}
		});
		
		viewpager.setCurrentItem(index);
	}

}
