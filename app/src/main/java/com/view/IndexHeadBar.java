package com.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.risen.androidwcc.R;

/**
 * TODO: document your custom view class.
 */
public class IndexHeadBar extends RelativeLayout {
    private String mExampleString;
    private int exampleSH;
    ImageView img_tab_now;

    public IndexHeadBar(Context context) {
        super(context);
        init(null, 0, context);
    }

    public IndexHeadBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0, context);
    }

    public IndexHeadBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle, context);
    }

    private void init(AttributeSet attrs, int defStyle, Context context) {
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_indexheadbar, null);
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.IndexHeadBar, defStyle, 0);
        mExampleString = a.getString(R.styleable.IndexHeadBar_exampleString);
        exampleSH = a.getInt(R.styleable.IndexHeadBar_exampleSH, 0);
        a.recycle();
        addView(view);
        ((TextView) view.findViewById(R.id.tvIndexBarTitle)).setText(mExampleString);
        img_tab_now = (ImageView) view.findViewById(R.id.img_tab_now);
        if (exampleSH == 0) {
            img_tab_now.setVisibility(View.INVISIBLE);
        } else {
            img_tab_now.setVisibility(View.VISIBLE);
        }
    }

    public void setBottomIvSH(int i) {
        img_tab_now.setVisibility(i);
    }
}
