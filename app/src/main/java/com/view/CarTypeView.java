package com.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.maincode.model.CarBrand;
import com.util.HttpUrl;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;

/**
 * 汽车品牌对应的类型视图
 *
 * @author djx
 */
public class CarTypeView extends LinearLayout {

    @ViewInject(R.id.listView)
    private ExpandableListView listView;
    private CarTypeViewAdapter adapter;
    private List<String> groupList = new ArrayList<String>();
    private List<CarSeries> chilidList = new ArrayList<CarSeries>();
    public CarSeriesCallBack back;
    CarBrand carBean;

    public CarTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setBack(CarSeriesCallBack back) {
        this.back = back;
    }

    private void init(Context context) {
        View currentLayout = LayoutInflater.from(context).inflate(R.layout.car_type_view, null);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        this.addView(currentLayout, params);
        ViewUtils.inject(this, currentLayout);
        listView.setGroupIndicator(null);
        Drawable childDivider = getContext().getResources().getDrawable(R.color.sepataor_line_color);
        listView.setChildDivider(childDivider);
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new CarTypeViewAdapter(groupList, chilidList, getContext());
            listView.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();
        for (int i = 0; i < groupList.size(); i++) {
            listView.expandGroup(i);
        }
        listView.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView arg0, View arg1, int arg2, long arg3) {
                return true;
            }
        });
        listView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2, int arg3, long arg4) {
                CarSeries value = chilidList.get(arg3);
                if (back != null) {
                    back.getSeries(value, carBean);
                }
                return true;
            }
        });
    }

    public void getCarSeries(CarBrand bean) {
        carBean = bean;
        com.util.xutil.HttpUtils utils = com.util.xutil.HttpUtils.getInstance();
        HttpParams params = new HttpParams(HttpUrl.GETBYBRAND);
        params.putString("brandId", bean.getId());
        String url = params.toString();

        utils.send(HttpMethod.GET, url, new HttpRequestCallBack<CarSeries>(CarSeries.class, getContext()) {

            @Override
            protected void onSuccess(CarSeries t) {
                chilidList.clear();
                groupList.clear();
                chilidList.addAll(t.getData());
//                for (int i = 0; i < chilidList.size(); i++) {
                groupList.add("");
//                }
                setAdapter();
            }
        });

    }

    public interface CarSeriesCallBack {
        void getSeries(CarSeries series, CarBrand carBrand);
    }

}
