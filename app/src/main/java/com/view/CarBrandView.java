package com.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.maincode.activity.Car.CarBrandAdapter;
import com.maincode.model.CarBrand;
import com.util.HttpUrl;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;

/**
 * 汽车品牌集合视图
 *
 * @author Administrator
 */
public class CarBrandView extends LinearLayout implements SectionIndexer {

    private ListView sortListView;
    private SideBar sideBar;
    private CarBrandAdapter adapter;
    private SlidingMenu slidingMenu;
    private List<CarBrand> SourceDateList = new ArrayList<CarBrand>();
    private CarTypeView carTypeView;

    public CarBrandView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setSlidingMenu(SlidingMenu slidingMenu) {
        this.slidingMenu = slidingMenu;
    }

    public void setCarTypeView(CarTypeView carTypeView) {
        this.carTypeView = carTypeView;
    }

    private void init(Context context) {
        View currentLayout = LayoutInflater.from(context).inflate(R.layout.car_brand_view, null);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        this.addView(currentLayout, params);
        ViewUtils.inject(this, currentLayout);
        initViews();
    }

    private void initViews() {
        sideBar = (SideBar) findViewById(R.id.sidrbar);
        // 设置右侧触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                // 该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    sortListView.setSelection(position);
                }
            }
        });

        sortListView = (ListView) findViewById(R.id.country_lvcountry);
        sortListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // 这里要利用adapter.getItem(position)来获取当前position所对应的对象
                CarBrand bean = adapter.getItem(position);
                if (slidingMenu.isOpen()) {
                    slidingMenu.closeMenu();
                } else {
                    slidingMenu.openMenu();
                    carTypeView.getCarSeries(bean);
                }
            }
        });
        adapter = new CarBrandAdapter(getContext(), SourceDateList);
        sortListView.setAdapter(adapter);
        getCarBrandFromHttp();
    }

    public void getCarBrandFromHttp() {
        com.util.xutil.HttpUtils utils = com.util.xutil.HttpUtils.getInstance();
        HttpParams params = new HttpParams(HttpUrl.CAR_BRAND);
        params.putString("start", 0 + "");
        params.putString("limit", Integer.MAX_VALUE + "");
        String url = params.toString();
        utils.send(HttpMethod.GET, url, new HttpRequestCallBack<CarBrand>(CarBrand.class, getContext()) {

            @Override
            protected void onSuccess(CarBrand t) {
                adapter.addData(t.getData());
            }
        });
    }

    @Override
    public Object[] getSections() {
        return null;
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    public int getSectionForPosition(int position) {
        return SourceDateList.get(position).getBfirstletter().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < SourceDateList.size(); i++) {
            String sortStr = SourceDateList.get(i).getBfirstletter();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }
}
