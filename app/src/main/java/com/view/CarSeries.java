package com.view;

import java.io.Serializable;
import java.util.List;

public class CarSeries implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String logo;
    private String name;
    public String result;
    private List<CarSeries> data;

    public List<CarSeries> getData() {
        return data;
    }

    public void setData(List<CarSeries> list) {
        this.data = list;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
