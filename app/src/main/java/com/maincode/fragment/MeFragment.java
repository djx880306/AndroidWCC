package com.maincode.fragment;

//import android.content.Context;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.activity.IndexActivity;
import com.maincode.activity.MeActivity;
import com.maincode.model.FullProfile;
import com.maincode.model.NearBean;
import com.maincode.model.OwnerCars;
import com.util.HttpUrl;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.RoundAngleImageView;

import java.util.List;

/**
 * 我
 */
public class MeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View v;
    IndexActivity activity;
    BitmapUtils bitmapUtils;

    @ViewInject(R.id.rimHead)
    RoundAngleImageView rimHead;
    @ViewInject(R.id.tvUserName)
    TextView tvUserName;
    @ViewInject(R.id.tvWccNum)
    TextView tvWccNum;
    @ViewInject(R.id.tvFrindNum)
    TextView tvFrindNum;
    @ViewInject(R.id.tvGZ)
    TextView tvGZ;
    @ViewInject(R.id.tvFacedNum)
    TextView tvFacedNum;
    @ViewInject(R.id.tvGroupNum)
    TextView tvGroupNum;
    @ViewInject(R.id.tvWhoLookMeNum)
    TextView tvWhoLookMeNum;
    @ViewInject(R.id.tvMyFristDynamic)
    TextView tvMyFristDynamic;
    @ViewInject(R.id.tvMyFristCar)
    TextView tvMyFristCar;
    @ViewInject(R.id.tvMyFristCertificates)
    TextView tvMyFristCertificates;
    @ViewInject(R.id.tvMyDynamicNum)
    TextView tvMyDynamicNum;
    @ViewInject(R.id.tvMyCarNum)
    TextView tvMyCarNum;
    @ViewInject(R.id.hsvDynamic)
    HorizontalScrollView hsvDynamic;
    @ViewInject(R.id.llDynamic)
    LinearLayout llDynamic;
    @ViewInject(R.id.hsvMyCar)
    HorizontalScrollView hsvMyCar;
    @ViewInject(R.id.llCar)
    LinearLayout llCar;

    public MeFragment() {
    }

    public static MeFragment newInstance(String param1) {
        MeFragment fragment = new MeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (IndexActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_me_index, container, false);
        ViewUtils.inject(this, v);
        getData();
        return v;
    }

    private void getData() {
        HttpParams httpParams = new HttpParams(HttpUrl.getUrl(HttpUrl.FULLPROFILE));
        HttpUtils httpUtils = HttpUtils.getInstance();
        httpUtils.send(HttpRequest.HttpMethod.GET, httpParams.toString(), new HttpRequestCallBack<FullProfile>(FullProfile.class, getActivity()) {
            @Override
            protected void onSuccess(FullProfile s) {
                bindView(s);
                clearViewedLog();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("数据加载失败");
            }
        });
    }

    /**
     * 数据绑定
     *
     * @param f
     */
    public void bindView(FullProfile f) {
        bitmapUtils = new BitmapUtils(activity);
        bitmapUtils.display(rimHead, HttpUrl.PICURL + "/" + f.data.user.defaultHeadsmall);
        tvUserName.setText(f.data.user.nicknameUnicode);
        tvWccNum.setText(f.data.user.id + "");
        tvFrindNum.setText(f.data.countFriender + "");
        tvGZ.setText(f.data.countFollow + "");
        tvFacedNum.setText(f.data.countFriender + "");
        tvGroupNum.setText(f.data.countCrowd + "");
        if (!(f.data.countBeviewedRecent + "").equals("null") & !(f.data.countBeviewedRecent + "").equals("0")) {
            tvWhoLookMeNum.setText(Html.fromHtml(f.data.countBeviewed + "<font color='red'>+" + f.data.countBeviewedRecent + "</font>"));
        } else {
            tvWhoLookMeNum.setText(f.data.countBeviewed + "");
        }
        tvMyDynamicNum.setText(f.data.countMoment + "");
        tvMyCarNum.setText(f.data.countCar + "");
        if (f.data.cars != null) {
            tvMyFristCar.setVisibility(View.GONE);
            hsvMyCar.setVisibility(View.VISIBLE);
            setChildViewCars(f.data.cars);
        }

        if (f.data.recentMoments != null) {
            tvMyFristDynamic.setVisibility(View.GONE);
            hsvDynamic.setVisibility(View.VISIBLE);
            setChildViewDynamic(f.data.recentMoments);
        }
    }

    /**
     * 添加我的爱车View
     *
     * @param cars
     */
    private void setChildViewCars(List<OwnerCars> cars) {
        for (int i = 0; i < (cars.size() > 3 ? 3 : cars.size()); i++) {
            ImageView iv = new ImageView(activity);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(140, RelativeLayout.LayoutParams.MATCH_PARENT);
            lp.leftMargin = 5;
            iv.setLayoutParams(lp);
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            llCar.addView(iv);
            bitmapUtils.display(iv, cars.get(i).seriesLogo);
        }
    }

    /**
     * 添加我的动态View
     *
     * @param nearBeen
     */
    private void setChildViewDynamic(List<NearBean> nearBeen) {
        for (int i = 0; i < (nearBeen.size() > 4 ? 4 : nearBeen.size()); i++) {
            ImageView iv = new ImageView(activity);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(105, RelativeLayout.LayoutParams.MATCH_PARENT);
            lp.leftMargin = 5;
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            iv.setLayoutParams(lp);
            llDynamic.addView(iv);
            bitmapUtils.display(iv, HttpUrl.PICURL + "/" + nearBeen.get(i).photos.get(0).originUri);
        }
    }

    /**
     * 清除新访问日志
     */
    private void clearViewedLog() {
        HttpParams hp = new HttpParams(HttpUrl.getUrl(HttpUrl.CLEARVIEWDLOG));
        HttpUtils httpUtils = HttpUtils.getInstance();
        httpUtils.send(HttpRequest.HttpMethod.GET, hp.toString(), new HttpRequestCallBack<String>(String.class, activity, false) {
            @Override
            protected void onSuccess(String s) {

            }
        });
    }

    @OnClick(R.id.rlHead)
    private void headOnclick(View v) {
        Intent intent = new Intent(activity, MeActivity.class);
        intent.putExtra(MeActivity.PARAMNAME, MeActivity.MEINFO);
        activity.startActivity(intent);
    }
}
