package com.maincode.fragment.login;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.WccApplication;
import com.maincode.activity.Car.CarListActivity;
import com.maincode.activity.IndexActivity;
import com.maincode.activity.SinglePageActivity;
import com.maincode.model.CarBrand;
import com.maincode.model.ImageObj;
import com.maincode.model.ImgBean;
import com.maincode.model.Result;
import com.maincode.model.UserInfo;
import com.util.HttpUrl;
import com.util.PictureHandler;
import com.util.SystemParams;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.CarSeries;
import com.view.HeadBar;
import com.view.MyPhotoDialog;
import com.view.RoundAngleImageView;
import com.view.TimerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 完善个人信息
 */
public class RegisterPerfectInfoFragment extends Fragment {
    View view;
    String sex = "0"; //性别
    String brithday; //生日
    String carBrandId;  //车辆品牌Id
    SinglePageActivity activity;
    BitmapUtils bitmapUtils;
    ImgBean imgBean;
    CarSeries series;
    CarBrand carBrand;

    @ViewInject(R.id.update_head)
    public RoundAngleImageView update_head;
    @ViewInject(R.id.etNickname)
    EditText etNickname;
    @ViewInject(R.id.sex_boy)
    TextView sex_boy;
    @ViewInject(R.id.sex_girl)
    TextView sex_girl;
    @ViewInject(R.id.user_birthday_msg)
    TextView user_birthday_msg;
    @ViewInject(R.id.tvcar)
    TextView tvcar;
    @ViewInject(R.id.headBar)
    HeadBar headBar;

    public RegisterPerfectInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register_perfect_info, container, false);
        activity = (SinglePageActivity) getActivity();
        ViewUtils.inject(this, view);
        bitmapUtils = new BitmapUtils(getActivity());
        imgBean = new ImgBean();
        headBar.setBarText("完善个人资料");
        headBar.setBarRightSH(View.INVISIBLE);
        return view;
    }

    @OnClick(R.id.go_back_tv)
    private void backClick(View v) {
        activity.backFragment();
    }

    /**
     * 头像赋值
     *
     * @param path
     */
    public void setHead(String path) {
        bitmapUtils.display(update_head, path);
        imgBean.setPath(path);
    }

    public void setCarBrand(CarSeries series, CarBrand carBrand) {
        tvcar.setText(series.getName());
        tvcar.setTextColor(getResources().getColor(R.color.tvtitle));
        this.series = series;
        this.carBrand = carBrand;
    }

    /**
     * 头像
     *
     * @param v
     */
    @OnClick(R.id.update_head)
    private void headClick(View v) {
        MyPhotoDialog dialog = new MyPhotoDialog.Builder(activity).setListener(
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // 拍照
                                activity.crameUtils.camera(activity);
                                dialog.dismiss();
                                break;
                            case 1: // 从相册选择
                                activity.crameUtils.album(activity);
                                dialog.dismiss();
                                break;

                        }
                    }
                }).create();
        dialog.show();
        LinearLayout contorl_layouts = (LinearLayout) dialog
                .findViewById(R.id.contorl_layouts);
        contorl_layouts.setVisibility(View.GONE);
    }

    /**
     * 开始玩车车
     *
     * @param v
     */
    @OnClick(R.id.tvComplete)
    private void completeClick(View v) {
        String nickName = etNickname.getText().toString().trim();
        if (imgBean.getPath() == null) {
            activity.showToast("请上传头像");
            return;
        }
        if (imgBean.getPath().equals("")) {
            activity.showToast("请上传头像");
            return;
        }
        if (nickName.equals("")) {
            activity.showToast("请填写昵称");
            return;
        }
        if (brithday == null) {
            activity.showToast("没有选择年龄");
            return;
        }
        if (brithday.equals("")) {
            activity.showToast("没有选择年龄");
            return;
        }
        HttpParams httpParams = new HttpParams(HttpUrl.REGISTBYSMSCODE);
        httpParams.putString("birthday", brithday);
        if (series != null & carBrand != null) {
            httpParams.putString("brandId", carBrand.getId());
            httpParams.putString("brandLogo", carBrand.getLogo());
            httpParams.putString("brandName", carBrand.getName() + "");
            httpParams.putString("seriesId", series.getId() + "");
            httpParams.putString("seriesLogo", series.getLogo() + "");
            httpParams.putString("seriesName", series.getName() + "");
        }
        httpParams.putString("gender", sex);
        httpParams.putString("nicknameUnicode", nickName);
        httpParams.putString("originPassword", activity.password);
        httpParams.putString("phone", activity.phone);
        httpParams.putString("smscode", activity.code);
        String url = httpParams.toString();
        HttpUtils httpUtils = HttpUtils.getInstance();
        HttpParams hp = new HttpParams(HttpUrl.REGISTBYSMSCODE);
        hp.addBodyParameter("pic", new File(imgBean.getPath()));

        httpUtils.send(HttpRequest.HttpMethod.POST, url, hp, new HttpRequestCallBack<String>(String.class, activity, "正在注册") {
            @Override
            protected void onSuccess(String s) {
                JSONObject jsonObject = JSONObject.parseObject(s);
                if (s.contains("true")) {
                    UserInfo userInfo = new UserInfo();
                    if (jsonObject.containsKey("data")) {
                        userInfo.token = jsonObject.getString("data");
                        activity.saveUserInfo(userInfo);
                        startActivity(new Intent(activity, IndexActivity.class));
                        activity.finish();
                    }
                } else {
                    activity.showToast(SystemParams.getInstance().getErrorMsg(jsonObject.getString("data")));
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("注册失败");
            }
        });
    }

    /**
     * 汽车品牌选择
     *
     * @param v
     */
    @OnClick(R.id.layoutCar)
    private void carClick(View v) {
        Intent intent0 = new Intent(activity, CarListActivity.class);
        activity.startActivityForResult(intent0, 301);
    }

    /**
     * 生日
     *
     * @param v
     */
    @OnClick(R.id.layoutDate)
    private void dateClick(View v) {
        TimerDialog.Buildler buildler = new TimerDialog.Buildler(activity);
        buildler.setBack(new TimerDialog.Buildler.OnDateCallBack() {
            @Override
            public void getDate(String year, String month, String day) {
                brithday = year + "-" + month + "-" + day;
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = (Date) formatter.parse(brithday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                brithday = (date.getTime() / 1000) + "";
                user_birthday_msg.setText(year + "-" + month + "-" + day);
                user_birthday_msg.setTextColor(getResources().getColor(R.color.tvtitle));
            }
        });
        TimerDialog dialog = buildler.create();
        dialog.show();
    }

    @OnClick(R.id.sex_boy)
    private void boyClick(View v) {
        sex = "0";
        sex_boy.setBackgroundResource(R.mipmap.icon_manselected);
        sex_girl.setBackgroundResource(R.mipmap.icon_female);
        sex_boy.setTextColor(Color.parseColor("#ffffff"));
        sex_girl.setTextColor(Color.parseColor("#FEB2C6"));
    }

    @OnClick(R.id.sex_girl)
    private void girlClick(View v) {
        sex = "1";
        sex_boy.setBackgroundResource(R.mipmap.icon_man);
        sex_girl.setBackgroundResource(R.mipmap.icon_femaleselected);
        sex_boy.setTextColor(Color.parseColor("#61D5FE"));
        sex_girl.setTextColor(Color.parseColor("#ffffff"));
    }
}
