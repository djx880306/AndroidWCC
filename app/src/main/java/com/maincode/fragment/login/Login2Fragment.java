package com.maincode.fragment.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.alibaba.fastjson.JSONObject;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.maincode.WCCBaseActivity;
import com.maincode.activity.SinglePageActivity;
import com.util.HttpUrl;
import com.util.SystemParams;
import com.util.Tools;
import com.util.volley.ReverseRegisterNetworkHelper;
import com.util.volley.UIDataListener;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.view.HeadBar;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Login2Fragment extends Fragment implements View.OnClickListener, UIDataListener {

    HeadBar headBar;
    View v;
    EditText edtPhone;
    SinglePageActivity activity;

    public Login2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (SinglePageActivity) getActivity();
        v = inflater.inflate(R.layout.fragment_login2, container, false);
        init();
        return v;
    }

    private void init() {
        headBar = (HeadBar) v.findViewById(R.id.headBar);
        headBar.onclick(this);
        headBar.setBarText("注册");
        headBar.setBarRightText("下一步");
        headBar.setBarRightSH(View.VISIBLE);
        edtPhone = (EditText) v.findViewById(R.id.edtPhone);
    }

    /**
     * 验证手机号码是否可用
     */
    private void setVerification() {
        HttpParams httpParams = new HttpParams(HttpUrl.AVAILABLEPHONE);
        httpParams.putString("phone", edtPhone.getText().toString().trim());
        HttpUtils httpUtils = com.util.xutil.HttpUtils.getInstance();
        httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<String>(String.class, getActivity(), "正在验证手机号...") {
            @Override
            protected void onSuccess(String s) {
                if (s.contains("true")) {
                    activity.fragmentManager("Login3", edtPhone.getText().toString().trim());
                } else {
                    JSONObject jsonObject = JSONObject.parseObject(s);
                    activity.showToast(SystemParams.getInstance().getErrorMsg(jsonObject.getString("data")));
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("手机号码不可用");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.go_back_tv:
                activity.backFragment();
                break;
            case R.id.tvBarRight:
                if (!Tools.isMobileNum(edtPhone.getText().toString().trim())) {
                    ((WCCBaseActivity) getActivity()).showToast("请输入正确的手机号码");
                } else {
                    setVerification();
                }
                break;
        }
    }

    @Override
    public void onDataChanged(Object data) {
        Log.i("Data", data.toString());
    }

    @Override
    public void onErrorHappened(String errorCode, String errorMessage) {

    }
}
