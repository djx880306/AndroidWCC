package com.maincode.fragment.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.activity.SinglePageActivity;
import com.maincode.activity.IndexActivity;
import com.maincode.model.UserInfo;
import com.util.HttpUrl;
import com.util.SystemParams;
import com.util.Tools;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.HeadBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View v;
    HeadBar headBar;
    SinglePageActivity activity;
    @ViewInject(R.id.login_phone_num)
    EditText login_phone_num;
    @ViewInject(R.id.login_code)
    EditText login_code;
    @ViewInject(R.id.login_btn)
    TextView login_btn;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_login, container, false);
        activity = (SinglePageActivity) getActivity();
        ViewUtils.inject(this, v);
        init();
        return v;
    }

    private void init() {
        headBar = (HeadBar) v.findViewById(R.id.headBar);
        headBar.setBarText("登录");
        headBar.setBarRightSH(View.VISIBLE);
        headBar.setBarRightBackGroundColor("#363636");
        headBar.setBarRightTextColor("#ffffff");
        headBar.setBarRightText("注册");
        headBar.setBackSH(View.INVISIBLE);
    }

    @OnClick(R.id.tvBarRight)
    private void onBarRight(View v) {
        activity.fragmentManager("Login3", "");
    }

    @OnClick(R.id.login_btn)
    private void loginClick(View v) {
        String phone = login_phone_num.getText().toString().trim();
        String pwd = login_code.getText().toString().trim();
        if (phone.length() == 0) {
            activity.showToast("请填写手机号");
            return;
        }
        if (!Tools.isMobileNum(phone)) {
            activity.showToast("手机号不正确");
            return;
        }
        if (pwd.length()==0){
            activity.showToast("请填写密码");
            return;
        }
        if (pwd.length() < 6) {
            activity.showToast("密码为6位以上");
            return;
        }
        HttpParams httpParams = new HttpParams(HttpUrl.LOGIN);
        httpParams.putString("phone", phone);
        httpParams.putString("originPassword", pwd);
        HttpUtils httpUtils = HttpUtils.getInstance();
        httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<String>(String.class, getActivity(), "正在登录...") {
            @Override
            protected void onSuccess(String s) {
                if (s.contains("true")) {
                    JSONObject jsonObject = JSONObject.parseObject(s);
                    UserInfo userInfo = new UserInfo();
                    if (jsonObject.containsKey("data")) {
                        userInfo.token = jsonObject.getString("data");
                        activity.saveUserInfo(userInfo);
                        activity.startActivity(new Intent(getActivity(), IndexActivity.class));
                        activity.finish();
                    } else {
                        activity.showToast(SystemParams.getInstance().getErrorMsg(jsonObject.getString("data")));
                    }
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("手机号或密码错误");
            }
        });
    }
}
