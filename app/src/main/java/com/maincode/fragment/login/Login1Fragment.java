package com.maincode.fragment.login;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.activity.SinglePageActivity;

/**
 * 第一个登陆界面
 */
public class Login1Fragment extends Fragment {
    SinglePageActivity activity;
    View v;

    public Login1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (SinglePageActivity) getActivity();
        v = inflater.inflate(R.layout.fragment_login1, container, false);
        ViewUtils.inject(this, v);
        return v;
    }

    @OnClick(R.id.tvLogin)
    private void loginClick(View v) {
        activity.fragmentManager("Login", "");
    }
}
