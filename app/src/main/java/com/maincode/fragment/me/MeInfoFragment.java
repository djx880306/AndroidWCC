package com.maincode.fragment.me;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.activity.MeActivity;

/**
 * 我-个人主页Fragment
 */
public class MeInfoFragment extends Fragment {
    View v;
    MeActivity activity;

    public MeInfoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_me_info, container, false);
        ViewUtils.inject(this, v);
        activity = (MeActivity) getActivity();
        return v;
    }

    @OnClick(R.id.tvBack)
    private void back(View v) {
        activity.finish();
    }
}
