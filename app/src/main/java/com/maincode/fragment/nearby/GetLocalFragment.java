package com.maincode.fragment.nearby;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baidu.mapapi.search.core.PoiInfo;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.maincode.WccApplication;
import com.maincode.activity.ActionActivity;
import com.maincode.model.LocalsBean;

import java.util.ArrayList;

/**
 * 获取地理位置
 */
public class GetLocalFragment extends Fragment implements AdapterView.OnItemClickListener {
    View v;
    ActionActivity activity;
    @ViewInject(R.id.review_list)
    private ListView local_list;
    private AdapterLocal adapter;
    private LocalsBean localsBean = new LocalsBean();

    ArrayList<PoiInfo> poiList;

    public GetLocalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_get_local, container, false);
        activity = (ActionActivity) getActivity();
        ViewUtils.inject(this, v);
        poiList = new ArrayList<PoiInfo>();
        if (((WccApplication) getActivity().getApplication()).poiList != null && ((WccApplication) getActivity().getApplication()).poiList.size() > 0) {
            poiList.addAll(((WccApplication) getActivity().getApplication()).poiList);
            initData();
        }
        activity.headBar.setBarText("所在位置");
        activity.headBar.setBarRightSH(View.INVISIBLE);
        adapter = new AdapterLocal(getActivity(), localsBean);
        local_list.setAdapter(adapter);
        local_list.setOnItemClickListener(this);
        return v;
    }

    private void initData() {
        LocalsBean.LocalBean localBean;
        for (int i = 0; i < poiList.size(); i++) {
            localBean = localsBean.new LocalBean();
            localBean.address = poiList.get(i).name;
            localBean.detail = poiList.get(i).address;
            localBean.selected = false;
            localBean.latLng = poiList.get(i).location;
            localsBean.locals.add(localBean);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        for (int i = 0; i < localsBean.locals.size(); i++) {
            localsBean.locals.get(position).selected = false;
        }
        localsBean.locals.get(position).selected = true;
        adapter.notifyDataSetChanged();
        activity.getLocalInfo(localsBean.locals.get(position).address, localsBean.locals.get(position).latLng);
        activity.backFragment();
    }
}
