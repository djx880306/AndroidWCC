package com.maincode.fragment.nearby;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import java.util.List;

/**
 * Created by djx on 16/1/19.
 */
public class NearbyViewPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> list;

    public NearbyViewPagerAdapter(List<Fragment> arrayFragment,FragmentManager fm) {
        super(fm);
        this.list = arrayFragment;
    }

    @Override
    public int getCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }
}
