package com.maincode.fragment.nearby;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.maincode.WCCBaseActivity;
import com.maincode.WccApplication;
import com.maincode.model.NearByPeople;
import com.maincode.model.Owner;
import com.util.HttpUrl;
import com.util.Tools;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.RoundAngleImageView;
import com.view.XListView;

import java.util.List;

/**
 * 附近-人
 */
public class PeopleFragment extends Fragment implements XListView.IXListViewListener {
    View v;
    int pager = 0;
    WCCBaseActivity activity;
    List<Owner> list;

    @ViewInject(R.id.xList)
    XListView xListView;

    public PeopleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_people, container, false);
        activity = (WCCBaseActivity) getActivity();
        ViewUtils.inject(this, v);
        xListView.setXListViewListener(this);
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        getData(true);
        return v;
    }

    /**
     * 获取附近人
     *
     * @param bl true为刷新 false为加载更多
     */
    private void getData(final boolean bl) {
        HttpParams httpParams = new HttpParams(HttpUrl.getUrl(HttpUrl.USERNEARBY));
        httpParams.putString("latitude", WccApplication.location.getLatitude() + "");
        httpParams.putString("longitude", WccApplication.location.getLongitude() + "");
        httpParams.putString("limit", "15");
        httpParams.putString("start", (pager * 15) + "");

        HttpUtils httpUtils = HttpUtils.getInstance();
        httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<NearByPeople>(NearByPeople.class, getActivity()) {
            @Override
            protected void onSuccess(NearByPeople nearByPeople) {
                if (bl | list == null) {
                    list = nearByPeople.records;
                } else {
                    list.addAll(nearByPeople.records);
                }
                if (list.size() < 15) {
                    xListView.setPullLoadEnable(false);
                }
                xListView.setAdapter(new PeopleAdapter());
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("获取数据失败");
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }
        });
    }

    @Override
    public void onRefresh() {
        pager = 0;
        getData(true);
    }

    @Override
    public void onLoadMore() {
        pager++;
        getData(false);
    }

    class PeopleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (list == null) return 0;
            else return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        class ViewHodler {
            RoundAngleImageView rivHead;
            RoundAngleImageView rivCarLog;
            RoundAngleImageView rivCarMLog;
            TextView tvUserName;
            TextView tvSex;
            TextView tvDate;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHodler viewHodler = new ViewHodler();
            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.layout_people_item, null);
                viewHodler.rivHead = (RoundAngleImageView) convertView.findViewById(R.id.rivHead);
                viewHodler.rivCarLog = (RoundAngleImageView) convertView.findViewById(R.id.rivCarLog);
                viewHodler.rivCarMLog = (RoundAngleImageView) convertView.findViewById(R.id.rivCarMLog);
                viewHodler.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
                viewHodler.tvSex = (TextView) convertView.findViewById(R.id.tvSex);
                viewHodler.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
                convertView.setTag(viewHodler);
            } else {
                viewHodler = (ViewHodler) convertView.getTag();
            }
            bindView(viewHodler, position);
            return convertView;
        }

        private void bindView(ViewHodler v, int p) {
            Owner owner = list.get(p);
            BitmapUtils bitmapUtils = new BitmapUtils(getActivity());
            bitmapUtils.display(v.rivHead, HttpUrl.PICURL + "/" + owner.defaultHeadsmall);
            bitmapUtils.display(v.rivCarLog, owner.seriesLogo);
            bitmapUtils.display(v.rivCarMLog, owner.brandLogo);
            v.tvUserName.setText(owner.nicknameUnicode);
            v.tvDate.setText(Tools.distance(Double.parseDouble(owner.distance)) + "  " + Tools.formatTime(owner.gmtModified + ""));
            v.tvSex.setBackgroundResource(owner.gender.equals("0") ? R.mipmap.bg_sex_male : R.mipmap.bg_sex_girl_bg);
            v.tvSex.setText(owner.age + "");
        }
    }
}
