package com.maincode.fragment.nearby;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.mapapi.map.Text;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.maincode.WCCBaseActivity;
import com.maincode.activity.ActionActivity;
import com.maincode.model.NearBean;
import com.maincode.model.NearbyBean;
import com.util.HttpUrl;
import com.util.SystemParams;
import com.util.Tools;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.RoundAngleImageView;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by djx on 16/1/22.
 * email dengjianxin09@163.com
 */
public class DynamicAdapter extends BaseAdapter {

    public List<NearBean> getRecords() {
        return records;
    }

    public void setRecords(List<NearBean> records) {
        this.records = records;
    }

    private List<NearBean> records;
    LayoutInflater layoutInflater;
    WCCBaseActivity context;

    public DynamicAdapter(Context context) {
        this.context = (WCCBaseActivity) context;
        layoutInflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        if (records == null)
            return 0;
        else
            return records.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHodler {
        public RoundAngleImageView rivHead;
        public ImageView ivCarHead;
        public ImageView ivCarLog;
        public ImageView ivDynamic;
        public TextView tvUserName;
        public TextView tvSex;
        public TextView tvDate;
        public TextView tvContent;
        public TextView tvLocal;
        public TextView tvZan;
        public TextView tvComment;
        public View vfgx;
        LinearLayout llLike, llComment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHodler v = new ViewHodler();
        if (convertView == null) {
            convertView = (View) layoutInflater.inflate(R.layout.layout_dynamic_item, null);
            v.rivHead = (RoundAngleImageView) convertView.findViewById(R.id.rivHead);
            v.ivCarHead = (ImageView) convertView.findViewById(R.id.ivCarHead);
            v.ivCarLog = (ImageView) convertView.findViewById(R.id.ivCarLog);
            v.ivDynamic = (ImageView) convertView.findViewById(R.id.ivDynamic);
            v.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
            v.tvSex = (TextView) convertView.findViewById(R.id.tvSex);
            v.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            v.tvContent = (TextView) convertView.findViewById(R.id.tvContent);
            v.tvLocal = (TextView) convertView.findViewById(R.id.tvLocal);
            v.tvZan = (TextView) convertView.findViewById(R.id.tvZan);
            v.tvComment = (TextView) convertView.findViewById(R.id.tvComment);
            v.vfgx = convertView.findViewById(R.id.vfgx);
            v.llLike = (LinearLayout) convertView.findViewById(R.id.llLike);
            v.llComment = (LinearLayout) convertView.findViewById(R.id.llComment);
            convertView.setTag(v);
        } else {
            v = (ViewHodler) convertView.getTag();
        }
        bindView(position, v);
        return convertView;
    }

    private void bindView(int p, final ViewHodler v) {
        final NearBean bean = records.get(p);
        BitmapUtils bitmapUtils = new BitmapUtils(context);
//        if (!bean.owner.defaultHeadsmall.equals("")) {
        bitmapUtils.display(v.rivHead, HttpUrl.PICURL + "/" + bean.owner.defaultHeadsmall);
//        } else {
//            v.rivHead.setBackgroundResource(R.mipmap.bg_wcc80);
//        }
        bitmapUtils.display(v.ivCarHead, bean.owner.seriesLogo);
        bitmapUtils.display(v.ivCarLog, bean.owner.brandLogo);
        bitmapUtils.display(v.ivDynamic, HttpUrl.PICURL + "/" + bean.photos.get(0).originUri);

        v.tvUserName.setText(bean.owner.nicknameUnicode);
        if (bean.contentUnicode != null) {
            v.tvContent.setText(bean.contentUnicode);
            v.tvContent.setVisibility(View.VISIBLE);
            v.vfgx.setVisibility(View.VISIBLE);
        } else {
            v.tvContent.setText("");
            v.tvContent.setVisibility(View.GONE);
            v.vfgx.setVisibility(View.GONE);
        }
        v.tvZan.setText(bean.likeCount + "");
//        likeClick(v.tvZan, bean.id + "");
        v.tvComment.setText(bean.commentCount + "");
        commentClick(v.llComment, bean.id + "");
        v.tvSex.setText(bean.owner.age + "");
        v.tvSex.setBackgroundResource(bean.owner.gender.equals("0") ? R.mipmap.bg_sex_male : R.mipmap.bg_sex_girl_bg);
        String str = "";
        if (!(bean.geoname + "").equals("") & !(bean.geoname + "").equals("null")) {
            str = bean.geoname + "--";
        }
        str += Tools.distance(Double.parseDouble(bean.distance));
        v.tvLocal.setText(str);
        v.tvDate.setText(Tools.formatTime(bean.gmtCreated + ""));
        v.llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bean.likeCount++;
                v.tvZan.setText(bean.likeCount + "");
                HttpParams httpParams = new HttpParams(HttpUrl.getUrl(HttpUrl.LIKE));
                httpParams.putString("momentsId", bean.id + "");
                HttpUtils httpUtils = HttpUtils.getInstance();
//                notifyDataSetChanged();
                httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<String>(String.class, context, false) {

                    @Override
                    protected void onSuccess(String s) {
                        if (s.contains("true")) {

                        } else {
//                            tvZan.setText((finalLikeNum--) + "");
//                            JSONObject jsonObject = JSON.parseObject(s);
//                            context.showToast(SystemParams.getInstance().getErrorMsg(jsonObject.getString("data")));
                        }
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        super.onFailure(arg0, arg1);
                        context.showToast("数据提交失败");
//                        tvZan.setText(finalLikeNum + "");
                    }
                });
            }
        });
    }

    /**
     * 评论的点击事件
     *
     * @param tvComment
     * @param param
     */
    private void commentClick(LinearLayout tvComment, final String param) {
        tvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WCCBaseActivity actionActivity = (WCCBaseActivity) context;

                Intent intent = new Intent(actionActivity, ActionActivity.class);
                intent.putExtra("dynamicId", param);
                intent.putExtra("fragmentIndex", ActionActivity.DYNAMICDETAIL);
                actionActivity.startActivity(intent);
            }
        });
    }
}
