package com.maincode.fragment.nearby;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.activity.ActionActivity;
import com.maincode.activity.IndexActivity;
import com.util.CrameUtils;
import com.view.IndexHeadBar;
import com.view.MyPhotoDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * 附近Fragment
 */
public class NearbyFragment extends Fragment implements ViewPager.OnPageChangeListener {
    List<Fragment> arrayFragment;
    DynamicFragment dynamicFragment;
    PeopleFragment peopleFragment;
    GroupsFragment groupsFragment;
    View view;
    MyPhotoDialog dialog;
    IndexActivity activity;

    @ViewInject(R.id.viewPager)
    ViewPager viewPager;
    @ViewInject(R.id.ihbAll)
    IndexHeadBar ihbAll;
    @ViewInject(R.id.ihbR)
    IndexHeadBar ihbR;
    @ViewInject(R.id.ihbQ)
    IndexHeadBar ihbQ;
    @ViewInject(R.id.right_btn1)
    ImageButton right_btn1;
    @ViewInject(R.id.right_btn2)
    Button right_btn2;
    @ViewInject(R.id.right_btn3)
    Button right_btn3;

    public NearbyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_nearby, container, false);
        ViewUtils.inject(this, view);
        activity = (IndexActivity) getActivity();
        addArrayFragment();
        return view;
    }

    private void addArrayFragment() {
        arrayFragment = new ArrayList<>();
        dynamicFragment = new DynamicFragment();
        peopleFragment = new PeopleFragment();
        groupsFragment = new GroupsFragment();
        arrayFragment.add(dynamicFragment);
        arrayFragment.add(peopleFragment);
        arrayFragment.add(groupsFragment);
        viewPager.setAdapter(new NearbyViewPagerAdapter(arrayFragment, getChildFragmentManager()));
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        setHeadViewSH(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @OnClick(R.id.right_btn1)
    private void addDynamicClick(View view) {

        dialog = new MyPhotoDialog.Builder(getActivity()).setListener(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // 拍照
                        activity.crameUtils.camera(activity);
                        dialog.dismiss();
                        break;
                    case 1: // 从相册选择
                        activity.crameUtils.album(activity);
                        dialog.dismiss();
                        break;
                    case 3: //取消
                        dialog.dismiss();
                        break;
                }
            }
        }).create();
        dialog.show();
        LinearLayout contorl_layouts = (LinearLayout) dialog.findViewById(R.id.contorl_layouts);
        contorl_layouts.setVisibility(View.GONE);
    }

    /**
     * 控制头部控件的显示隐藏
     *
     * @param p
     */
    private void setHeadViewSH(int p) {
        if (p == 1) {
            ihbAll.setBottomIvSH(View.INVISIBLE);
            ihbQ.setBottomIvSH(View.INVISIBLE);
            ihbR.setBottomIvSH(View.VISIBLE);

            right_btn1.setVisibility(View.INVISIBLE);
            right_btn2.setVisibility(View.VISIBLE);
            right_btn3.setVisibility(View.INVISIBLE);
        } else if (p == 0) {
            ihbAll.setBottomIvSH(View.VISIBLE);
            ihbQ.setBottomIvSH(View.INVISIBLE);
            ihbR.setBottomIvSH(View.INVISIBLE);

            right_btn1.setVisibility(View.VISIBLE);
            right_btn2.setVisibility(View.INVISIBLE);
            right_btn3.setVisibility(View.INVISIBLE);
        } else if (p == 2) {
            ihbAll.setBottomIvSH(View.INVISIBLE);
            ihbQ.setBottomIvSH(View.VISIBLE);
            ihbR.setBottomIvSH(View.INVISIBLE);

            right_btn1.setVisibility(View.INVISIBLE);
            right_btn2.setVisibility(View.INVISIBLE);
            right_btn3.setVisibility(View.VISIBLE);
        }
    }
}
