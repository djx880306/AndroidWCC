package com.maincode.fragment.nearby;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.model.LatLng;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.WccApplication;
import com.maincode.activity.ActionActivity;
import com.maincode.model.ImageObj;
import com.maincode.model.ImgBean;
import com.maincode.model.ImgUploadBean;
import com.maincode.model.Result;
import com.maincode.model.UploadBean;
import com.util.HttpUrl;
import com.util.PictureHandler;
import com.util.RoundImgHorizontalAdapter;
import com.util.Tools;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.GrideViewImgListView;

import java.util.ArrayList;
import java.util.List;

/**
 * 添加动态
 */
public class AddDynamicFragment extends Fragment {
    View v;
    ActionActivity activity;
    ImgBean imgBean;

    @ViewInject(R.id.local_btn)
    public Button local_btn;
    @ViewInject(R.id.content)
    EditText content;
    @ViewInject(R.id.tvTextSize)
    TextView tvTextSize;
    @ViewInject(R.id.ivDynamic)
    ImageView ivDynamic;
    @ViewInject(R.id.tvSelectedLocal)
    TextView tvSelectedLocal;
    @ViewInject(R.id.ivRight)
    ImageView ivRight;

    public AddDynamicFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_dynamic, container, false);
        activity = (ActionActivity) getActivity();
        ViewUtils.inject(this, v);
        imgBean = new ImgBean();
        if (!activity.address.equals("")) {
            tvSelectedLocal.setText(activity.address);
//            ivRight.setVisibility(View.GONE);
        }
        init();
        TextSizeChange();
        return v;
    }

    private void init() {
        activity.headBar.setVisibility(View.VISIBLE);
        activity.headBar.setBarText("发布动态");
        activity.headBar.setBarRightSH(View.VISIBLE);
        activity.headBar.setBarRightText("发布");

        Bundle bundle = getArguments();
        if (bundle != null) {
            String path = bundle.getString("Path");
            BitmapUtils bitmapUtils = new BitmapUtils(getActivity());
            bitmapUtils.display(ivDynamic, path);
            imgBean.setPath(path);
        }
    }

    /**
     * 改变动态下面可输入的字数
     */
    private void TextSizeChange() {
        content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvTextSize.setText((500 - s.toString().length()) + "");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 发布动态
     *
     * @param v
     */
    public void addDynamic(String v) {
        HttpParams httpParams = new HttpParams(HttpUrl.getUrl(HttpUrl.SAVE));
//        if (content.getText().toString().trim().length() > 0) {
        String txt = Tools.urlEncoder(content.getText() + "");
        httpParams.putString("contentUnicode", txt);
//        }
//        else {
//            activity.showToast("请输入动态内容");
//            return;
//        }
//        if (activity.address.equals("")) {
//            activity.showToast("请选择所在地");
//            return;
//        }
        httpParams.putString("geoname", activity.address + "");
        httpParams.putString("lat", ((WccApplication) activity.getApplication()).location.getLatitude() + "");
        httpParams.putString("lng", ((WccApplication) activity.getApplication()).location.getLongitude() + "");
        httpParams.putString("photosUrl", v);
        httpParams.putString("viewLevel", "0");

        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<String>(String.class, getActivity()) {
            @Override
            protected void onSuccess(String s) {
                if (s.contains("content")) {
                    activity.isRefresh = true;
                    activity.backFragment();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                activity.showToast("发布失败");
            }
        });
    }

    /**
     * 上传图片
     */
    public void upLoadImg() {
        if ((imgBean.getPath() + "").equals("null")) {
            activity.showToast("请选择动态图片");
            return;
        }
//        if (content.getText().toString().trim().length() == 0) {
//            activity.showToast("此时此刻请说点什么");
//            return;
//        }
        PictureHandler handler = new PictureHandler(getActivity()) {

            @Override
            public void onSuccess(Result value) {
//                addDynamic(value.getData());
            }

            @Override
            public void onSuccess(ImageObj value) {
                addDynamic(value.data.get(0).originUri);
            }
        };
        List<ImgBean> tempList = new ArrayList<ImgBean>();
        if (!(imgBean.getPath() + "").equals("null")) {
            tempList.add(imgBean);
            handler.handlerPicture(tempList, 480, 840, WccApplication.cacheFile.getPath());
        }
    }

    @OnClick(R.id.rlLocal)
    private void getLocal(View v) {
        activity.fragmentManager(ActionActivity.LOCAL);
    }
}
