package com.maincode.fragment.nearby;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.risen.androidwcc.R;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.maincode.WccApplication;
import com.maincode.model.NearBean;
import com.maincode.model.NearbyBean;
import com.util.HttpUrl;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.XListView;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * 附近-动态
 */
public class DynamicFragment extends Fragment implements XListView.IXListViewListener {
    View view;
    @ViewInject(R.id.xList)
    XListView xList;
    DynamicAdapter adapter;
    public List<NearBean> records;
    int pageNum = 0;

    public DynamicFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dynamic, container, false);
        ViewUtils.inject(this, view);
        adapter = new DynamicAdapter(getActivity());
        xList.setAdapter(adapter);
        setXListInfo();
        getData(0);
        return view;
    }

    private void setXListInfo() {
        xList.setXListViewListener(this);
        xList.setPullLoadEnable(true);
        xList.setPullRefreshEnable(true);
    }

    /**
     * 获取服务器的数据
     *
     * @param state 为0时表示刷新 否则为加载更多
     */
    private void getData(final int state) {
        HttpParams httpParams = new HttpParams(HttpUrl.getUrl(HttpUrl.NEARBY));
        httpParams.putString("latitude", WccApplication.location.getLatitude() + "");
        httpParams.putString("longitude", WccApplication.location.getLongitude() + "");
        httpParams.putString("start", (pageNum * 10) + "");
        httpParams.putString("limit", "10");
        HttpUtils httpUtils = HttpUtils.getInstance();
//        Type nearbyType = new TypeToken<List<NearbyBean>>() {}.getClass();
        boolean bl = false;
        if (state == 0)
            bl = false;
        else
            bl = true;
        httpUtils.send(HttpRequest.HttpMethod.POST, httpParams.toString(), new HttpRequestCallBack<NearbyBean>(NearbyBean.class, getActivity(), bl) {
            @Override
            protected void onSuccess(NearbyBean nearbyBeen) {
                Log.i("nearbyBeen", nearbyBeen.toString() + "");
                if (records == null | state == 0) {
                    records = nearbyBeen.records;
                } else {
                    records.addAll(nearbyBeen.records);
                }
                if (nearbyBeen.records.size() < 10) {
                    xList.setPullLoadEnable(false);
                }
                adapter.setRecords(records);
                adapter.notifyDataSetChanged();
                xList.stopRefresh();
                xList.stopLoadMore();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                super.onFailure(arg0, arg1);
                Log.i("nearbyBeen", arg1 + "");
                xList.stopRefresh();
                xList.stopLoadMore();
            }
        });
    }

    @Override
    public void onRefresh() {
        pageNum = 0;
        getData(0);
    }

    @Override
    public void onLoadMore() {
        pageNum++;
        getData(1);
    }
}
