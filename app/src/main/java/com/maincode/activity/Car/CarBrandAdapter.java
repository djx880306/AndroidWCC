package com.maincode.activity.Car;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.example.risen.androidwcc.R;
import com.maincode.model.CarBrand;
import com.util.ViewHolder;
import com.view.AdapterManager;

public class CarBrandAdapter extends AdapterManager<CarBrand> implements SectionIndexer {

	public CarBrandAdapter(Context mContext, List<CarBrand> list) {
		super(list, mContext);
	}
	public View getView(final int position, View view, ViewGroup arg2) {
		if (view == null) {
			view = this.inflater.inflate(R.layout.activity_group_member_item, null);
		}
		ImageView img_head = ViewHolder.get(view, R.id.img_head);
		TextView tvTitle = ViewHolder.get(view, R.id.title);
		TextView tvBank = ViewHolder.get(view, R.id.catalog);
		final CarBrand mContent = list.get(position);
		if(!TextUtils.isEmpty(mContent.getLogo())){
			bu.display(img_head, mContent.getLogo());
		}
		// 根据position获取分类的首字母的Char ascii值
		int section = getSectionForPosition(position);
		// 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
		if (position == getPositionForSection(section)) {
			tvBank.setVisibility(View.VISIBLE);
			tvBank.setText(mContent.getBfirstletter());
		} else {
			tvBank.setVisibility(View.GONE);
		}
		tvTitle.setText(this.list.get(position).getName());
		tvTitle.setTextSize(16);
		return view;

	}

	/**
	 * 根据ListView的当前位置获取分类的首字母的Char ascii值
	 */
	public int getSectionForPosition(int position) {
		return list.get(position).getBfirstletter().charAt(0);
	}

	/**
	 * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
	 */
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = list.get(i).getBfirstletter();
			char firstChar = sortStr.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

	@Override
	public Object[] getSections() {
		return null;
	}
}
