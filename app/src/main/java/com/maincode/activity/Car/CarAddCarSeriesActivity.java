package com.maincode.activity.Car;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.WCCBaseActivity;
import com.maincode.model.Result;
import com.util.HttpUrl;
import com.util.MenuStrategy;
import com.util.Tools;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.util.xutil.HttpUtils;
import com.view.HeadBar;

/**
 * 添加车型
 *
 * @author Administrator
 */
public class CarAddCarSeriesActivity extends WCCBaseActivity {
    @ViewInject(R.id.edt_carName)
    private EditText edt_carName;
    @ViewInject(R.id.headView)
    HeadBar headView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_add_car_series_activity);
//        initializeHead("返回", "提交车型", "确定");
//        setMenuStrategy(this);
        ViewUtils.inject(this);
        headView.setBarText("提交车型");
        headView.setBarRightText("确定");
    }

    @OnClick(R.id.tvBarRight)
    public void openActivity(View v) {
        if (!Tools.StringHasContent(edt_carName.getText().toString())) {
            Toast.makeText(this, "请输入车型名称", Toast.LENGTH_SHORT).show();
            return;
        }
        HttpUtils utils = HttpUtils.getInstance();
        HttpParams params = new HttpParams(HttpUrl.ADDCAR);
        params.putString("name", edt_carName.getText().toString());
        utils.send(HttpMethod.GET, params.toString(), new HttpRequestCallBack<Result>(Result.class, this) {

            @Override
            protected void onSuccess(Result t) {
                if ("0".equals(t.getCode())) {
                    Toast.makeText(CarAddCarSeriesActivity.this, "车型添加成功", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(CarAddCarSeriesActivity.this, "车型添加失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
