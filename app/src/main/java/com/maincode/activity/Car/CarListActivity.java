package com.maincode.activity.Car;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.WCCBaseActivity;
import com.maincode.model.CarBrand;
import com.util.MenuStrategy;
import com.util.SystemParams;
import com.util.Tools;
import com.view.CarBrandView;
import com.view.CarSeries;
import com.view.CarTypeView;
import com.view.HeadBar;
import com.view.SlidingMenu;

/**
 * 汽车品牌和型号界面
 *
 * @author Administrator
 */
public class CarListActivity extends WCCBaseActivity {
    // 汽车品牌
    @ViewInject(R.id.car_brand)
    private CarBrandView carBrandView;
    // 汽车品牌对应的汽车类型
    @ViewInject(R.id.car_type)
    private CarTypeView carTypeView;
    @ViewInject(R.id.layout_slidingMenu)
    private SlidingMenu slidingMenu;
    @ViewInject(R.id.headView)
    HeadBar headView;
    public static final int CAR_SERVIES_RESBONSE_CODE = 301;

    public static final int ADD_CAR_TYPE = 308;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.car_list_activity);
        ViewUtils.inject(this);
        if (getIntent().getBooleanExtra("is_near", false)) {
            headView.setBarText("车型");
            headView.setBarRightSH(View.INVISIBLE);
        } else {
            String title = Tools.StringHasContent(getIntent().getStringExtra(SystemParams.NAME)) ? getIntent().getStringExtra(SystemParams.NAME) : "车型";
            headView.setBarText(title);
            headView.setBarRightSH(View.VISIBLE);
            headView.setBarRightText("没有我的车型?");
            headView.setBarRightTxtSize(12);
            headView.setBarRightTextColor("#3ABAFA");
            headView.setBarRightBackGroundColor("#363636");
        }

        carBrandView.setSlidingMenu(slidingMenu);
        carBrandView.setCarTypeView(carTypeView);
        carTypeView.setBack(new CarTypeView.CarSeriesCallBack() {

            @Override
            public void getSeries(CarSeries series, CarBrand carBrand) {
                Intent intent = new Intent();
                intent.putExtra(SystemParams.START_FOR_RESULT_Bean, series);
                intent.putExtra(SystemParams.BRANDINFO, carBrand);
                setResult(CAR_SERVIES_RESBONSE_CODE, intent);
                finish();
            }
        });
    }

    @OnClick(R.id.go_back_tv)
    private void backClick(View v) {
        finish();
    }

    @OnClick(R.id.tvBarRight)
    public void openActivity(View v) {
        Intent intent = new Intent(this, CarAddCarSeriesActivity.class);
        this.startActivityForResult(intent, ADD_CAR_TYPE);
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        super.onActivityResult(arg0, arg1, arg2);
        switch (arg0) {
            case ADD_CAR_TYPE:
                if (slidingMenu.isOpen()) {
                    slidingMenu.closeMenu();
                    carBrandView.getCarBrandFromHttp();
                }
                break;

            default:
                break;
        }
    }
}
