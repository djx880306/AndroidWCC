package com.maincode.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.http.client.HttpRequest;
import com.maincode.WCCBaseActivity;
import com.maincode.fragment.login.Login1Fragment;
import com.maincode.fragment.login.Login2Fragment;
import com.maincode.fragment.login.Login3Fragment;
import com.maincode.fragment.login.LoginFragment;
import com.maincode.fragment.login.RegisterPerfectInfoFragment;
import com.maincode.model.CarBrand;
import com.maincode.model.UserInfo;
import com.util.CrameUtils;
import com.util.HttpUrl;
import com.util.SystemParams;
import com.util.xutil.HttpParams;
import com.util.xutil.HttpRequestCallBack;
import com.view.CarSeries;

import java.io.File;
import java.net.URI;

/**
 * 单页Activity 注册用的
 */
public class SinglePageActivity extends WCCBaseActivity {
    String tag = "";//用来判断启动哪个Fragment
    String barTitle = ""; //头部Bar的显示内容
    public CrameUtils crameUtils;
    public String path = "";
    static RegisterPerfectInfoFragment registerPerfectInfoFragment;
    public String password = "";
    public String code = "";
    public String phone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singlepage);
        crameUtils = new CrameUtils();
        tag = "Login";
        fragmentManager(tag, "");
        getParam();
        isToken();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        super.onSaveInstanceState(outState, outPersistentState);
    }

    /**
     * 判断token是否存在并且更新token
     */
    private void isToken() {
        if (!token.equals("")) {
//            HttpParams httpParams = new HttpParams(HttpUrl.REFRESHTOKEN);
//            httpParams.putString("t", token);
//            com.util.xutil.HttpUtils httpUtils = com.util.xutil.HttpUtils.getInstance();
//            httpUtils.send(HttpRequest.HttpMethod.GET, httpParams.toString(), new HttpRequestCallBack<String>(String.class, this, "数据处理中...") {
//                @Override
//                protected void onSuccess(String s) {
//                    if (s.contains("true")) {
//                        JSONObject jsonObject = JSON.parseObject(s);
//                        if (jsonObject.containsKey("data")) {
//                            UserInfo userInfo = new UserInfo();
//                            userInfo.token = jsonObject.getString("data");
//                            saveUserInfo(userInfo);
//                        }
//                    }
//                }
//            });
            startActivity(new Intent(SinglePageActivity.this, IndexActivity.class));
            finish();
        }
    }

    /**
     * 获取参数
     */
    private void getParam() {
        Intent intent = getIntent();
        if (intent.hasExtra("tag")) {
            tag = intent.getStringExtra("tag");
        } else {
//            headBar.setVisibility(View.GONE);
        }
        if (intent.hasExtra("barTitle")) {
            barTitle = intent.getStringExtra("barTitle");
//            headBar.setBarText(barTitle);
        }
    }

    public void fragmentManager(String tag, String param) {
        if (tag.equals("")) {
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, new Login1Fragment(), "Login1")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null)
                    .commit();
        } else if (tag.equals("Login2")) {
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, new Login2Fragment(), "Login2")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null)
                    .commit();
        } else if (tag.equals("Login3")) {
            Login3Fragment fragment = new Login3Fragment();
            Bundle bundle = new Bundle();
            bundle.putString("phone", param);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, fragment, "Login3")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null)
                    .commit();
        } else if (tag.equals("Login")) {
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, new LoginFragment(), "Login")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null)
                    .commit();
        } else if (tag.equals("RegisterPerfectInfo")) {
            registerPerfectInfoFragment = new RegisterPerfectInfoFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, registerPerfectInfoFragment, "Login")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null)
                    .commit();
        }
    }

    public void backFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.go_back_tv:  //返回按钮
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    tag = "";
                    fragmentManager("", "");
                }
                break;
            case R.id.btnRegistered: //注册按钮
                tag = "Login2";
                fragmentManager("Login2", "");
                break;
            case R.id.ivWeixin:
                break;
            case R.id.ivQQ:
                break;
            case R.id.ivSin:
                break;
            case R.id.tvLogin:
                tag = "Login";
                fragmentManager("Login", "");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CrameUtils.CAMERA:
                if (crameUtils.saveFile != null) {
                    path = crameUtils.saveFile.getAbsolutePath();
                    File file = new File(path);
                    if (!file.exists()) {
                        return;
                    } else {
                        crameUtils.cropImage(this, Uri.fromFile(file), 160, 160, 100, false);
                    }
                } else {
                    return;
                }
                break;
            case CrameUtils.ALBUM:
                if (data == null) {
                    return;
                } else if (data.getData() == null) {
                    return;
                } else {
                    path = crameUtils.getPath(this, data.getData());
                    crameUtils.cropImage(this, data.getData(), 160, 160, 100, false);
                }
                break;
            case 301:
                if (data != null) {
                    CarSeries series = (CarSeries) data.getSerializableExtra(SystemParams.START_FOR_RESULT_Bean);
                    CarBrand carBrand = (CarBrand) data.getSerializableExtra(SystemParams.BRANDINFO);
                    if (series == null) return;
                    registerPerfectInfoFragment.setCarBrand(series, carBrand);
                }
                break;
            case 100:
                if (resultCode == RESULT_OK) {
                    if (crameUtils.saveFile.exists()) {
                        path = crameUtils.saveFile.getAbsolutePath();
                    } else {
                        path = "";
                    }
                } else {
                    path = "";
                }
                break;
        }
        Log.e("path", path);
        if (!path.equals("") & path != null & registerPerfectInfoFragment != null & (requestCode == 100 | requestCode == 101)) {
            registerPerfectInfoFragment.setHead(path);
        }
    }

    @Override
    public void onBackPressed() {
        backFragment();
    }
}
