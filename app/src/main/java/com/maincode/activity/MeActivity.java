package com.maincode.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.example.risen.androidwcc.R;
import com.maincode.WCCBaseActivity;
import com.maincode.fragment.me.MeInfoFragment;

public class MeActivity extends WCCBaseActivity {
    public static final int MEINFO = 1000;  //个人基本信息
    public static final String PARAMNAME = "MEPARAM";
    MeInfoFragment meInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        Intent intent = getIntent();
        if (intent != null) {
            fragmentManager(intent.getIntExtra(PARAMNAME, 1000));
        }
    }

    public void fragmentManager(int i) {
        switch (i) {
            case MEINFO:
                meInfoFragment = new MeInfoFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.rlChildFragment, meInfoFragment).commitAllowingStateLoss();
                break;
        }
    }

}
