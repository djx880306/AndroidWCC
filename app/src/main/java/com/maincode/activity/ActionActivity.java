package com.maincode.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.baidu.mapapi.model.LatLng;
import com.example.risen.androidwcc.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.maincode.WCCBaseActivity;
import com.maincode.fragment.nearby.AddDynamicFragment;
import com.maincode.fragment.nearby.DynamicCommentFragment;
import com.maincode.fragment.nearby.GetLocalFragment;
import com.util.CrameUtils;
import com.view.HeadBar;
import com.view.ImagesListView;
import com.view.MyPhotoDialog;

import java.io.File;

public class ActionActivity extends WCCBaseActivity {
    AddDynamicFragment addDynamicFragment;
    DynamicCommentFragment dynamicCommentFragment;
    GetLocalFragment getLocalFragment;
    public static final int ADDDYNAMIC = 1000; //添加动态
    public static final int LOCAL = 1001;  //获取定位信息
    public static final int DYNAMICDETAIL = 1002;  //附近-动态-评论详情
    public String param = "";
    private int state;
    public boolean isRefresh = false;
    MyPhotoDialog dialog;

    @ViewInject(R.id.headBar)
    public HeadBar headBar;
    public String address = "";
    private CrameUtils crameUtils;
    Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        ViewUtils.inject(this);
        headBar.setVisibility(View.GONE);
        crameUtils = new CrameUtils();
        Intent intent = getIntent();
        if (intent.hasExtra("dynamicId")) {
            param = intent.getStringExtra("dynamicId");
        }
        int index = intent.getIntExtra("fragmentIndex", 0);
        if (index == 0) {
            showToast("数据处理失败，请重试");
            finish();
        } else {
            fragmentManager(index);
        }
    }

    public void fragmentManager(int i) {
        state = i;
        switch (i) {
            case ADDDYNAMIC:
                addDynamicFragment = new AddDynamicFragment();
                bundle.putString("Path", getIntent().getStringExtra("Path"));
                addDynamicFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, addDynamicFragment).addToBackStack(null)
                        .commitAllowingStateLoss();
                break;
            case LOCAL:
                getLocalFragment = new GetLocalFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, getLocalFragment).addToBackStack(null)
                        .commitAllowingStateLoss();
                headBar.setBarText("所在位置");
                headBar.setBarRightSH(View.INVISIBLE);
                break;
            case DYNAMICDETAIL:
                dynamicCommentFragment = new DynamicCommentFragment();
                if (!param.equals("")) {
                    bundle.putString("DynamicId", param);
                    dynamicCommentFragment.setArguments(bundle);
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, dynamicCommentFragment).addToBackStack(null)
                        .commitAllowingStateLoss();
                break;
        }
    }

    public void getLocalInfo(String address, LatLng latLng) {
        if (!address.equals("") & addDynamicFragment != null) {
            this.address = address;
        }
    }

    @OnClick(R.id.go_back_tv)
    private void backClick(View v) {
//        showDialog("您确定放弃此次操作嘛？");
        backFragment();
    }

    public void backFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (state == ADDDYNAMIC) {
            finish();
        } else {
            backFragment();
        }
    }

    @OnClick(R.id.tvBarRight)
    private void barRightClick(View v) {
        if (addDynamicFragment != null) {
            addDynamicFragment.upLoadImg();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        addDynamicFragment = new AddDynamicFragment();
        String path = "";
        switch (requestCode) {
            case CrameUtils.CAMERA:
                if (crameUtils.saveFile != null) {
                    path = crameUtils.saveFile.getAbsolutePath();
                    if (!new File(path).exists()) {
                        return;
                    }
                } else {
                    return;
                }
                break;
            case CrameUtils.ALBUM:
                if (data == null) {
                    return;
                } else {
                    path = crameUtils.getPath(this, data.getData());
                }
                break;
        }
        addDynamicFragment = new AddDynamicFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Path", path);
        addDynamicFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, addDynamicFragment).addToBackStack(null)
                .commitAllowingStateLoss();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            //获取当前获得当前焦点所在View
            View view = getCurrentFocus();
            if (isClickEt(view, event)) {

                //如果不是edittext，则隐藏键盘
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    if (dynamicCommentFragment != null) {
                        if (dynamicCommentFragment.commentState == -1) {
                            //隐藏键盘
                            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            dynamicCommentFragment.etCommentContent.setHint("请输入评论");
                            dynamicCommentFragment.etCommentContent.setText("");
                            dynamicCommentFragment.replayToCommentId = "";
                            dynamicCommentFragment.replayToUid = "";
                        } else {
                            dynamicCommentFragment.commentState = -1;
                        }
                    }
                }
            }
            return super.dispatchTouchEvent(event);
        }
        /**
         * 看源码可知superDispatchTouchEvent  是个抽象方法，用于自定义的Window
         * 此处目的是为了继续将事件由dispatchTouchEvent(MotionEvent event)传递到onTouchEvent(MotionEvent event)
         * 必不可少，否则所有组件都不能触发 onTouchEvent(MotionEvent event)
         */
        if (getWindow().superDispatchTouchEvent(event)) {
            return true;
        }
        return onTouchEvent(event);
    }

    /**
     * 获取当前点击位置是否为et
     *
     * @param view  焦点所在View
     * @param event 触摸事件
     * @return
     */
    public boolean isClickEt(View view, MotionEvent event) {
        if (view != null && ((view instanceof EditText) | (view instanceof Button))) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            view.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            //此处根据输入框左上位置和宽高获得右下位置
            int bottom = top + view.getHeight();
            int right = left + view.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
