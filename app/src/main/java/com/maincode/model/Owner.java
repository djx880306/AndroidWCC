package com.maincode.model;

/**
 * Created by djx on 16/1/21.
 * email dengjianxin09@163.com
 */
public  class Owner {
    public String address;
    public int age;
    public String backgroundUrl;
    public long birthday;
    public String carLogo;
    public String carName;
    public String city;
    public String cityName;
    public String company;
    public String defaultHeadsmall;
    public String distance;
    public String emotion;
    public String gender;
    public int getmsg;
    public long gmtCreated;
    public long gmtModified;
    public String headsmall;
    public int id;
    public String job;
    public String lat;
    public String license;
    public int licenseRejectid;
    public int licenseStatus;
    public String lng;
    public long logtime;
    public String nickname;
    public String nicknameUnicode;
    public String openfire;
    public String phone;
    public String province;
    public String provinceName;
    public String qqAuth;
    public String seriesId;
    public int show;
    public String sign;
    public String sinaAuth;
    public String sort;
    public String soundUrl;
    public int status;
    public String verify;
    public String zodiac;
    public String seriesLogo;
    public String brandLogo;
}