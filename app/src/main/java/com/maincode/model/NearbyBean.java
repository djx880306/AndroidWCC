package com.maincode.model;

import java.util.List;

/**
 * Created by djx on 16/1/21.
 * email dengjianxin09@163.com
 */
public class NearbyBean {
    public int start;
    public int limit;
    public String dir;
    public List<NearBean> records;
}
