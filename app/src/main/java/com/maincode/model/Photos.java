package com.maincode.model;

/**
 * Created by djx on 16/1/21.
 * email dengjianxin09@163.com
 */
public class Photos {
    public long gmtCreated;
    public long gmtModified;
    public int id;
    public int momentsId;
    public String originUri;
}
