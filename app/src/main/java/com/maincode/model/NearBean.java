package com.maincode.model;

import java.util.List;

/**
 * Created by djx on 16/1/22.
 * email dengjianxin09@163.com
 */
public class NearBean {
    public int commentCount;
    public int likeCount;
    public String content;
    public String contentUnicode;
    public long gmtCreated;
    public long gmtModified;
    public int id;
    public double lat;
    public double lng;
    public int uid;
    public int viewLevel;
    public String distance;
    public String geoname;
    public Owner owner;
    public List<Photos> photos;
}