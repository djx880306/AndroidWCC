package com.maincode.model;

/**
 * Created by djx on 16/1/21.
 * email dengjianxin09@163.com
 */
public class ImgUploadBean {
    public String originUri;
    public String rename;
    public String path;
    public String fullPath;
    public String error;
}
