package com.maincode.model;

import java.util.List;

/**
 * Created by djx on 16/1/27.
 * email dengjianxin09@163.com
 */
public class NearByPeople {
    public String dir;
    public int limit;
    public int start;
    public int totals;
    public List<Owner> records;
}
