package com.maincode.model;

import java.io.Serializable;
import java.util.List;

public class CarBrand implements Serializable {
    private String id;
    private String name;
    private String logo;
    private String bfirstletter;
    private String gmtCreated;
    private String gmtModified;
    public String result;
    private List<CarBrand> data;


    public List<CarBrand> getData() {
        return data;
    }

    public void setData(List<CarBrand> data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBfirstletter() {
        return bfirstletter;
    }

    public void setBfirstletter(String bfirstletter) {
        this.bfirstletter = bfirstletter;
    }

    public String getGmt_created() {
        return gmtCreated;
    }

    public void setGmt_created(String gmt_created) {
        this.gmtCreated = gmt_created;
    }

    public String getGmt_modified() {
        return gmtModified;
    }

    public void setGmt_modified(String gmt_modified) {
        this.gmtModified = gmt_modified;
    }

}
