package com.maincode.model;

/**
 * Created by djx on 16/2/1.
 * email dengjianxin09@163.com
 */
public class OwnerCars {
    public String id;
    public String gmtCreated;
    public String gmtModified;
    public String uid;
    public String phone;
    public String cardUrl;
    public String platnumber;
    public String brand;
    public String seriesId;
    public String seriesName;
    public String seriesLogo;
    public String color;
    public String emissions;
    public String shift;
    public String brandLogo;
    public String brandId;
}
