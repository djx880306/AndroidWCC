package com.maincode.model;

import com.baidu.mapapi.model.LatLng;

import java.util.ArrayList;

/**
 * Created by djx on 16/1/20.
 * email dengjianxin09@163.com
 */
public class LocalsBean {
    public ArrayList<LocalBean> locals = new ArrayList<LocalsBean.LocalBean>();

    public class LocalBean {
        public String id;
        public String address;
        public String detail;
        public boolean selected;
        public LatLng latLng;
    }
}
