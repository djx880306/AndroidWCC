package com.maincode.model;

import java.util.List;

/**
 * Created by djx on 16/1/25.
 * email dengjianxin09@163.com
 */
public class CommentBean {
    public boolean result;
    public List<Comment> data;

    public class Comment {
        public String content;
        public String contentUnicode;
        public long gmtCreated;
        public long gmtModified;
        public int id;
        public int momentsId;
        public int type;
        public int uid;
        public Owner owner;
        public Owner replayToUser;
    }
}
