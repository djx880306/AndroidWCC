package com.maincode.model;

import java.util.ArrayList;
import java.util.List;

public class UploadBean {
	public List<UploadBean> data = new ArrayList<UploadBean>();
	private String filename;
	public String path;
	public String name;
	
	private String msg;
	private String debugMsg;
	private String code;
	private String page;
	
	
	public List<UploadBean> getData() {
		return data;
	}
	public void setData(List<UploadBean> data) {
		this.data = data;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDebugMsg() {
		return debugMsg;
	}
	public void setDebugMsg(String debugMsg) {
		this.debugMsg = debugMsg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	
	
}	
