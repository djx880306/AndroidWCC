package com.maincode.model;

import java.util.List;

/**
 * 首页-我的信息
 * Created by djx on 16/2/2.
 * email dengjianxin09@163.com
 */
public class FullProfile {
    public String result;
    public FullProfile data;
    public List<OwnerCars> cars;
    public String distance;
    public int countFollow;
    public int countBeviewed;
    public int countFriender;
    public int countFollower;
    public int countCrowd;
    public int countBeviewedRecent;
    public int countMoment;
    public int countCar;
    public List<NearBean> recentMoments;
    public Owner user;
}
